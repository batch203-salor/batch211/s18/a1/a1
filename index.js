// Add
function printSum(num1, num2) {
  let sum = num1 + num2;
  return sum;
}
console.log("Displayed sum of 5 and 15");
console.log(printSum(5, 15));
// Subtract
function printDifference(num1, num2) {
  let difference = num1 - num2;
  return difference;
}
console.log("Displayed difference of 20 and 5");
console.log(printDifference(20, 5));
// Multiply
function printProduct(num1, num2) {
  let product = num1 * num2;
  return product;
}
// Divide
console.log("Displayed product of 50 and 10");
console.log(printProduct(50, 10));
function printQuotient(num1, num2) {
  let quotient = num1 / num2;
  return quotient;
}

console.log("Displayed quotient of 50 and 10");
console.log(printQuotient(50, 10));

// Area of Circle
function printAreaOfCircle(num1) {
  let area = Math.floor(Math.PI * num1 * num1);
  return area;
}
console.log("The result of getting the area of a circle with 15 radius: ");
console.log(printAreaOfCircle(15));
// Print Average
function printAverage(num1, num2, num3, num4) {
  let total = num1 + num2 + num3 + num4;
  let average = total / 4;
  return average;
}
console.log("The average of 20,40,60 and 80: ");
console.log(printAverage(20, 40, 60, 80));
// Check for passing score.
function printCheckIfPassed(score, total) {
  let percentage = 75;
  let result = (score / total) * 100;
  let isPassingScore = result >= percentage;
  return isPassingScore;
}
console.log("Is 38/50 a passing score?");
console.log(printCheckIfPassed(38, 50));
